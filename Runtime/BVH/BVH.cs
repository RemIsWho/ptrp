﻿using System.Collections.Generic;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using static Unity.Mathematics.math;
using Rem.Rendering.PathTracing;

namespace Rem.Rendering.BVH
{
    public enum Axis : byte
    {
        None = 0,
        X = 1,
        Y = 2,
        Z = 4,
        All = X | Y | Z
    }
    public enum EntityType { Sphere, Light, Mesh, Voxel }
    [System.Serializable]
    public struct EntityData
    {
        public float3 pos;
        public AABB bounds;
        public EntityType type;
        public int index;
    }
    [System.Serializable]
    public struct BvhNodeData
    {
        public int ID;
        public int LeftID;
        public int RightID;
        public int ParentID;
        public int Depth;
    }
    public struct EntityBoundsComparer : IComparer<EntityData>
    {
        private readonly Axis axis;

        public EntityBoundsComparer(Axis axis) => this.axis = axis;

        public int Compare(EntityData lhs, EntityData rhs)
        {
            return (int)sign(lhs.bounds.Min[(int)axis] - rhs.bounds.Min[(int)axis]);
        }
    }

    public unsafe struct BvhNode
    {
        public readonly AABB bounds;
        public readonly BvhNode* left;
        public readonly BvhNode* right;
        public readonly BvhNodeData* data;

        public BvhNode(NativeSlice<EntityData> entities, NativeList<BvhNode> nodes, NativeList<BvhNodeData> datas, int depth = 0, int parent = 0)
        {
            left = null;
            right = null;

            datas.AddNoResize(default);
            data = (BvhNodeData*)datas.GetUnsafePtr() + datas.Length - 1;

            var entireBounds = AABB.Identity;
            foreach (EntityData entity in entities)
                entireBounds = AABB.Enclose(entireBounds, entity.bounds);

            var biggestPartition = Axis.None;
            var biggestPartitionSize = float.MinValue;
            for (int partition = 1; partition < 3; partition++)
            {
                float size = entireBounds.Size[partition];
                if (size > biggestPartitionSize)
                {
                    biggestPartition = (Axis)partition;
                    biggestPartitionSize = size;
                }
            }

            entities.Sort(new EntityBoundsComparer(biggestPartition));

            switch (entities.Length)
            {
                case 1:
                    //EntityId = entities[0].Id;
                    bounds = entities[0].bounds;
                    break;

                default:
                    //EntityId = -1;

                    int partitionLength = 0;
                    float partitionStart = entities[0].bounds.Min[(int)biggestPartition];

                    // decide the size of the partition according to the size of the entities
                    for (var i = 0; i < entities.Length; i++)
                    {
                        EntityData entity = entities[i];
                        partitionLength++;
                        AABB bounds = entity.bounds;
                        if (bounds.Min[(int)biggestPartition] - partitionStart > biggestPartitionSize / 2 ||
                            bounds.Size[(int)biggestPartition] > biggestPartitionSize / 2)
                        {
                            break;
                        }
                    }

                    // ensure we have at least 1 entity in each partition
                    if (partitionLength == entities.Length)
                        partitionLength--;

                    var leftNode = new BvhNode(new NativeSlice<EntityData>(entities, 0, partitionLength), nodes, datas,
                        depth + 1, data->ID);
                    data->LeftID = leftNode.data->ID = nodes.Length;
                    nodes.AddNoResize(leftNode);

                    var rightNode = new BvhNode(new NativeSlice<EntityData>(entities, partitionLength), nodes, datas,
                        depth + 1, data->ID);
                    data->RightID = rightNode.data->ID = nodes.Length;
                    nodes.AddNoResize(rightNode);

                    bounds = AABB.Enclose(leftNode.bounds, rightNode.bounds);
                    break;
            }
        }
    }
}
