﻿using Rem.Rendering.PathTracing;
using Unity.Mathematics;
using UnityEngine;

namespace Rem.PathTracing
{
    public class Additional : MonoBehaviour
    {
        //used by meshes
        [HideInInspector] public AABB prevBounds; 
        [HideInInspector] public Matrix4x4 prevWL; 
        [HideInInspector] public Matrix4x4 prevLW;
        private new MeshRenderer renderer; 

        // only used by sphere (and probably lights)
        [HideInInspector] public float3 prevPos; 

        public void UpdateMatrix()
        {
            if (renderer == null)
                renderer = GetComponent<MeshRenderer>();

            if (renderer != null)
            {
                prevBounds = new AABB(renderer.bounds.min, renderer.bounds.max);
            }
            prevWL = transform.worldToLocalMatrix;
            prevLW = transform.localToWorldMatrix;
            prevPos = transform.position;
        }
    }
}
