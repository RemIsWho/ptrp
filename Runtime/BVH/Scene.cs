﻿using System;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using static Rem.Common.NativeCollectionUtility;
using HashList = System.Collections.Generic.List<int>;
using Rem.Rendering.PathTracing;
using Rem.PathTracing;
using UnityEngine;

namespace Rem.Rendering.BVH
{
    public class Scene : IDisposable
    {
        #region Fields
        readonly RTRenderPipeline m_RenderPipeline;
        public NativeList<EntityData> Entities;
        public HashList entityIDs;
        public NativeList<PathTracing.Material> Materials;
        public HashList matIDs;

        public NativeList<PathTracing.Light> Lights;
        public NativeList<Sphere> Spheres;

        public NativeList<MeshObject> Meshes;
        public HashList meshIDs;
        public NativeList<float3> Vertexes;
        public NativeList<float3> Normals;
        public NativeList<float2> UVs;
        public NativeList<int> Indices;
        #endregion

        #region Constructor
        public Scene(RTRenderPipeline renderPipeline)
        {
            m_RenderPipeline = renderPipeline;
            Entities = new NativeList<EntityData>(Allocator.Persistent);
            Materials = new NativeList<PathTracing.Material>(Allocator.Persistent);
            Lights = new NativeList<PathTracing.Light>(Allocator.Persistent);
            Spheres = new NativeList<Sphere>(Allocator.Persistent);

            Meshes = new NativeList<MeshObject>(Allocator.Persistent);
            Vertexes = new NativeList<float3>(Allocator.Persistent);
            Normals = new NativeList<float3>(Allocator.Persistent);
            UVs = new NativeList<float2>(Allocator.Persistent);
            Indices = new NativeList<int>(Allocator.Persistent);

            entityIDs = new HashList();
            matIDs = new HashList();
            meshIDs = new HashList();
        }
        #endregion

        #region Methods
        public void SetupScene(UnityEngine.SceneManagement.Scene scene)
        {
            UnityEngine.GameObject[] roots = scene.GetRootGameObjects();
            foreach (var go in roots)
            {
                if (!go.activeInHierarchy)
                    continue;

                var light = go.GetComponent<UnityEngine.Light>();
                Add(light);

                var renderer = go.GetComponent<UnityEngine.Renderer>();
                if (renderer != null && renderer.sharedMaterial.shader.name == RTRenderPipeline.AllowedShader)
                {
                    var mf = go.GetComponent<UnityEngine.MeshFilter>();
                    if (mf == null)
                        continue;
                    var info = go.GetComponent<Additional>();
                    if (info == null)
                    {
                        info = go.AddComponent<Additional>();
                        info.UpdateMatrix();
                    }

                    if (mf.sharedMesh.name == "Sphere")
                    {
                        Add(new Sphere() { Pos = go.transform.position, prevPos = info.prevPos, Scale = go.transform.lossyScale.x / 2f }, renderer.sharedMaterial, go.GetInstanceID());
                        info.UpdateMatrix(); // update, after adding
                    }
                    else
                    {
                        Add(mf, renderer, info, go);
                        info.UpdateMatrix(); // update, after adding
                    }
                }
            }
        }
        internal void Add(Sphere sphere, UnityEngine.Material mat, int entityID)
        {
            sphere.Material = AddMat(mat);
            Spheres.Add(sphere);
            Entities.Add(new EntityData { pos = sphere.Pos, bounds = new AABB(sphere.Pos, sphere.Scale), index = Spheres.Length - 1, type = EntityType.Sphere });
            entityIDs.Add(entityID);
        }

        internal void Add(UnityEngine.Light light)
        {
            if (light == null)
                return;

            var info = light.gameObject.GetComponent<Additional>();
            if (info == null)
            {
                info = light.gameObject.AddComponent<Additional>();
                info.UpdateMatrix();
            }

            switch (light.type)
            {
                case UnityEngine.LightType.Point:
                    Lights.Add(new PathTracing.Light()
                    {
                        Type = (uint)PathTracing.LightType.Point,
                        Pos = light.transform.position,
                        prevPos = info.prevPos,
                        Color = ColorUtils.ColorToFloat3(light.color) * light.intensity,
                        Scale = light.range / 10f
                    });
                    break;

                case UnityEngine.LightType.Area:
                    Lights.Add(new PathTracing.Light()
                    {
                        Type = (uint)PathTracing.LightType.Area,
                        Pos = light.transform.position,
                        prevPos = info.prevPos,
                        Dir = light.transform.forward,
                        Color = ColorUtils.ColorToFloat3(light.color) * light.intensity,
                        Scale = light.range / 10f
                    });
                    break;

                case UnityEngine.LightType.Directional:
                    Lights.Add(new PathTracing.Light()
                    {
                        Type = (uint)PathTracing.LightType.Direction,
                        Pos = light.transform.position,
                        prevPos = info.prevPos,
                        Dir = light.transform.forward,
                        Color = ColorUtils.ColorToFloat3(light.color) * light.intensity,
                        Scale = light.range / 10f
                    });
                    break;
            }
            Entities.Add(new EntityData { pos = light.transform.position, bounds = new AABB(light.transform.position, light.range / 10f), index = Lights.Length - 1, type = EntityType.Light });
            entityIDs.Add(light.gameObject.GetInstanceID());
            info.UpdateMatrix(); // update, after adding
        }
        internal void Add(PathTracing.Light light, int entityID)
        {
            Lights.Add(light);
            Entities.Add(new EntityData { pos = light.Pos, bounds = new AABB(light.Pos, light.Scale), index = Lights.Length - 1, type = EntityType.Light });
            entityIDs.Add(entityID);
        }
        internal unsafe void Add(UnityEngine.MeshFilter mf, UnityEngine.Renderer renderer, Additional info, UnityEngine.GameObject go)
        {
            var obj = new MeshObject()
            {
                Bounds = new AABB(renderer.bounds.min, renderer.bounds.max),
                prevBounds = info.prevBounds,
                WorldToLocal = go.transform.worldToLocalMatrix,
                LocalToWorld = go.transform.localToWorldMatrix,
                prevWorldToLocal = info.prevWL,
                prevLocalToWorld = info.prevLW,
                Material = AddMat(renderer.sharedMaterial)
            };

            //add mesh and set the vertex values.
            int index = meshIDs.IndexOf(mf.sharedMesh.GetInstanceID());
            meshIDs.Add(mf.sharedMesh.GetInstanceID());
            if (index != -1)
            {
                obj.IndexStart = Meshes[index].IndexStart;
                obj.IndexLength = Meshes[index].IndexLength;
            }
            else
            {
                int firstVertex = Vertexes.Length;
                Vertexes.AddRange(mf.sharedMesh.vertices.GetUnsafePointer(), mf.sharedMesh.vertexCount);
                Normals.AddRange(mf.sharedMesh.normals.GetUnsafePointer(), mf.sharedMesh.vertexCount);
                UVs.AddRange(mf.sharedMesh.uv.GetUnsafePointer(), mf.sharedMesh.vertexCount);

                // Add index data - if the vertex buffer wasn't empty before, the
                // indices need to be offset
                int firstIndex = Indices.Length;
                var trils = mf.sharedMesh.GetIndices(0);
                int start = Indices.Length;

                Indices.AddRange(trils);
                var job = new UpdateIndicesJob
                {
                    Indicies = new NativeSlice<int>(Indices, start, trils.Length),
                    Offset = firstVertex
                }.Schedule(trils.Length, 64);
                job.Complete();

                obj.IndexStart = firstIndex;
                obj.IndexLength = trils.Length;
            }

            //lastly add this meshobject
            Meshes.Add(obj);
            Entities.Add(new EntityData { bounds = Meshes[Meshes.Length - 1].Bounds, index = Meshes.Length - 1, pos = go.transform.position, type = EntityType.Mesh });
            entityIDs.Add(go.GetInstanceID());
        }
        [BurstCompile]
        private struct UpdateIndicesJob : IJobParallelFor
        {
            public NativeSlice<int> Indicies;
            public int Offset;
            public void Execute(int index)
            {
                Indicies[index] += Offset;
            }
        }
        private int AddMat(UnityEngine.Material mat)
        {
            int index = matIDs.IndexOf(mat.GetInstanceID());
            if (index != -1)
                return index;
            else
            {
                PathTracing.Material myMat = m_RenderPipeline.GetMat(mat);
                Materials.Add(myMat);
                matIDs.Add(mat.GetInstanceID());
                return Materials.Length - 1;
            }
        }
        #endregion

        #region BVH
        public NativeList<BvhNodeData> GetBvhNodeDatas()
        {
            NativeList<BvhNodeData> nodeDatas = new NativeList<BvhNodeData>(Entities.Length, Allocator.Persistent);
            NativeList<BvhNode> nodes = new NativeList<BvhNode>(Entities.Length, Allocator.Persistent);
            //todo

            return nodeDatas;
        }
        #endregion

        #region IDisposable
        public void Dispose()
        {
            entityIDs = null;
            matIDs = null;
            meshIDs = null;

            if (Entities.IsCreated) Entities.Dispose();
            if (Materials.IsCreated) Materials.Dispose();
            if (Lights.IsCreated) Lights.Dispose();
            if (Spheres.IsCreated) Spheres.Dispose();

            if (Meshes.IsCreated) Meshes.Dispose();
            if (Vertexes.IsCreated) Vertexes.Dispose();
            if (Normals.IsCreated) Normals.Dispose();
            if (UVs.IsCreated) UVs.Dispose();
            if (Indices.IsCreated) Indices.Dispose();
        }
        #endregion
    }
}
