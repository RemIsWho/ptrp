﻿using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace Rem.Rendering.PathTracing
{
	/// <summary>
	/// Small Struct that contains the AABB box of a mesh, means we can first check the AABB before checking any triangles for intersection
	/// </summary>
	[System.Serializable]
	public struct AABB
    {
		public readonly float3 Min, Max;

		#region Properties
		public float3 Center => Min + (Max - Min) / 2;
		public float3 Size => Max - Min;
		public float3[] Corners => new float3[]
		{
			new float3(Min.x, Min.y, Min.z),
			new float3(Min.x, Min.y, Max.z),
			new float3(Min.x, Max.y, Min.z),
			new float3(Max.x, Min.y, Min.z),
			new float3(Min.x, Max.y, Max.z),
			new float3(Max.x, Max.y, Min.z),
			new float3(Max.x, Min.y, Max.z),
			new float3(Max.x, Max.y, Max.z),
		};
		public static AABB Identity => new AABB(new float3(float.MaxValue, float.MaxValue, float.MaxValue), new float3(float.MinValue, float.MinValue, float.MinValue));
        #endregion

        #region Constructors
        public AABB(float3 min, float3 max)
		{
			Min = min;
			if (max.x <= min.x) max.x = min.x + 0.01f;
			if (max.y <= min.y) max.y = min.y + 0.01f;
			if (max.z <= min.z) max.z = min.z + 0.01f;
			Max = max;
		}
		public AABB(float3 pos, float radius)
		{
			Min = pos - float3(radius, radius, radius);
			Max = pos + float3(radius, radius, radius);
		}
        #endregion

        #region Methods
        public bool Hit(float3 rayOrigin, float3 rayDirection, float tMin, float tMax)
		{
			float3 rayInvDirection = rcp(rayDirection);
			float3 t0 = (Min - rayOrigin) * rayInvDirection;
			float3 t1 = (Max - rayOrigin) * rayInvDirection;

			tMin = max(tMin, cmax(min(t0, t1)));
			tMax = min(tMax, cmin(max(t0, t1)));

			return tMin < tMax;
		}

		public static AABB Enclose(AABB lhs, AABB rhs)
		{
			return new AABB(min(lhs.Min, rhs.Min), max(lhs.Max, rhs.Max));
		}
        #endregion
    }
}
