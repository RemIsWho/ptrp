static const float PI = 3.14159265;
static const float PI2 = 6.28318530718;
static const float PHI = 1.618033988749894;
static const float EPSILON = 1e-8;

//-----------------------------
//- RAY

struct Ray
{
	float3 origin;
	float3 direction;
};
Ray CreateRay(float3 origin, float3 direction)
{
    Ray ray;
    ray.origin = origin;
    ray.direction = direction;
    return ray;
}

struct RayHit
{
	float distance;
    float2 uv;
    int material; // or color id
    bool hasUV;
};
RayHit CreateHit(float distance, int mat)
{
    RayHit m;
    m.distance = distance;
    m.uv = 0;
    m.material = mat;
    m.hasUV = false;
    return m;
};

//--------------------------------
//- SCENE

#define MatLambert 0
#define MatSpecular 1
#define MatDielectric 2
#define MatEmissive 3
#define MatVolume 4

#define LightPoint 0
#define LightDirectional 1
#define LightArea 2

struct Material
{
    uint type;
    float3 albedo;
    float roughness;
    float IOR;
    float normal;
    float2 scaling;
    int textureID;
    int NormalTextureID;
    int MetalTextureID;
};

struct Light
{
    uint type;
    float3 pos;
    float3 prevPos;
    float3 dir;
    float scale;
    float3 col;
};

struct Sphere
{
    float3 pos;
    float3 prevPos;
    float scale;
    int mat;
};

//----------------------------------
//- MESHES
struct AABB
{
    float3 min;
    float3 max;
};

float cmin(float3 f)
{
    if (f.x < f.y)
    {
        if(f.x < f.z)
            return f.x;
        else
            return f.z;
    }
    else
    {
        if(f.y < f.z)
            return f.y;
        else 
            return f.z;
    }
}
float cmax(float3 f)
{
    if (f.x > f.y)
    {
        if (f.x > f.z)
            return f.x;
        else
            return f.z;
    }
    else
    {
        if (f.y > f.z)
            return f.y;
        else
            return f.z;
    }
}
bool Intersect(in float3 ro, in float3 rINV, in AABB bounds, float tMin, float tMax)
{
    float3 t0 = (bounds.min - ro) * rINV;
    float3 t1 = (bounds.max - ro) * rINV;

    tMin = max(tMin, cmax(min(t0, t1)));
    tMax = min(tMax, cmin(max(t0, t1)));

    return tMin < tMax;
}
bool IntersectPlane(in float3 ro, in float3 rd, in float3 lo, in float3 ld, out float t)
{
    float denom = dot(ld, rd);
    if (denom > 1e-6)
    {
        float3 p0l0 = lo - ro;
        t = dot(p0l0, ld) / denom;
        return (t >= 0);
    }
 
    return false;
}
float IntersectDisk(in float3 ro, in float3 rd, in float3 lo, in float3 ld, in float radius)
{
    float t = 0;
    if (IntersectPlane(ro, rd, lo, ld, t))
    {
        float3 p = ro + rd * t;
        float3 v = p - lo;
        float d2 = dot(v, v);
        if(sqrt(d2) <= radius)
            return t;
        // or you can use the following optimisation (and precompute radius^2)
        // return d2 <= radius2; // where radius2 = radius * radius
    }
 
    return 1e20;
}

bool IntersectTriangle_MT97(in float3 ro, in float3 rd, float3 vert0, float3 vert1, float3 vert2, inout float t, inout float u, inout float v)
{
	// find vectors for two edges sharing vert0
    float3 edge1 = vert1 - vert0;
    float3 edge2 = vert2 - vert0;

	// begin calculating determinant - also used to calculate U parameter
    float3 pvec = cross(rd, edge2);

	// if determinant is near zero, ray lies in plane of triangle
    float det = dot(edge1, pvec);

	// use backface culling
    if (det < EPSILON) // && det > -EPSILON)
        return false;
    float inv_det = 1.0f / det;

	// calculate distance from vert0 to ray origin
    float3 tvec = ro - vert0;

	// calculate U parameter and test bounds
    u = dot(tvec, pvec) * inv_det;
    if (u < 0.0 || u > 1.0f)
        return false;

	// prepare to test V parameter
    float3 qvec = cross(tvec, edge1);

	// calculate V parameter and test bounds
    v = dot(rd, qvec) * inv_det;
    if (v < 0.0 || u + v > 1.0f)
        return false;

	// calculate t, ray intersects triangle
    t = dot(edge2, qvec) * inv_det;
    return true;
}

struct MeshObject
{
    AABB bounds;
    AABB prevbounds;
    float4x4 worldToLocal;
    float4x4 localToWorld;
    float4x4 prevWorldToLocal;
    float4x4 prevLocalToWorld;
    int indexStart;
    int indexLength;
    int mat;
};

//-----------------------
//- REPROJECTION
struct motionHit
{
    float distance;
    uint type;
    uint objectID;
    uint triangleID;
};
bool Equal(motionHit a, motionHit b)
{
    return a.type == b.type &&
           a.objectID == b.objectID && a.triangleID == b.triangleID;
}

//------------------------
//- COLORS
float3 hsv2rgb(float3 c)
{
    float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}