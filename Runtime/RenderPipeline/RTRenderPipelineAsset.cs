﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Rem.Rendering
{
    [CreateAssetMenu(menuName = "Rendering/Path-Tracing RenderPipeline")]
    public class RTRenderPipelineAsset : RenderPipelineAsset
    {
        public int DebugStuffs;

        #region Fields
        [Header("Shader")]
        public ComputeShader RayTracingShader;
        public ComputeShader AntiAliasingShader;
        public bool UseAA;
        [HideInInspector] public Material _PostEffectMat;
        [Space(5)]
        public Texture2D SkyboxTexture;
        public Texture2D BlueNoiseTexture;
        [Header("Misc")]
        public bool ECS_ONLY_INGAME = false;
        public bool ForceUpdateScene = true;

        [Header("Settings")]
        public float LOD_DISTANCE = 7f;
        [Range(1, 4)] public int SAMPLES_PER_PIXEL = 1;
        [Range(1, 8)] public int BOUNCES_PER_SAMPLE = 4;
        [Range(1, 8)] public int BOUNCES_WITH_LIGHT = 4;
        [Header("TXAA")]
        [Range(0.001f, 1f)] public float MATRIX_JITTER = 0.45f;
        [Range(10, 60)] public int REPROJ_MAXFRAMES = 30;

        [Header("Post Processing")]
        [Range(0.1f, 3f)] public float Exposure = 1f;
        [Range(0f, 8f)] public float VignettePower = 1.1f;
        [Range(0f, 4f)] public float AberrationOffset = 0.25f;
        #endregion

        #region Methods
        protected override RenderPipeline CreatePipeline()
        {
            #region Validate
            if (BOUNCES_WITH_LIGHT > BOUNCES_PER_SAMPLE)
                BOUNCES_WITH_LIGHT = BOUNCES_PER_SAMPLE;
            #endregion

            #region Set Settings
            _PostEffectMat = new Material(Shader.Find("Hidden/PostEffect"));
            _PostEffectMat.SetFloat("_Exposure", Exposure);
            _PostEffectMat.SetFloat("_VignettePower", VignettePower);
            _PostEffectMat.SetFloat("_AberrationOffset", AberrationOffset);

            RayTracingShader.SetFloat("_DistanceLOD", 1f / LOD_DISTANCE);

            RayTracingShader.SetTexture(0, "_BlueNoise", BlueNoiseTexture);
            RayTracingShader.SetTexture(0, "_SkyboxTexture", SkyboxTexture);

            RayTracingShader.SetInt("SAMPLES_PER_PIXEL", SAMPLES_PER_PIXEL);
            RayTracingShader.SetInt("BOUNCES_PER_SAMPLE", BOUNCES_PER_SAMPLE);
            RayTracingShader.SetInt("BOUNCES_WITH_LIGHT", BOUNCES_WITH_LIGHT);

            RayTracingShader.SetInt("REPROJ_MAXFRAMES", REPROJ_MAXFRAMES);
            #endregion

            return new RTRenderPipeline(this);
        }
        #endregion
    }
}