﻿Shader "PTRP/Standard"
{
    Properties
    {
        [KeywordEnum(Lambert, Specular, Dielectric, Emissive, Volume)] _MatType("Material Type", Float) = 0
        [HDR] _Albedo("Main Color", Color) = (1,1,1)

        [MainTexture] _MainTex("Albedo (RGB)", 2D) = "white" {}
        [NoScaleOffset] _NormalTex("Normal (RGB)", 2D) = "white" {}
        [NoScaleOffset] _MetalTex("Metal (R)", 2D) = "white" {}

        [Normal] _Normal("Normal Strength", Range(0.00, 1.00)) = 1.0
        _Roughness("Roughness", Range(0.00, 1.00)) = 0.5
        _IOR("IOR", Range(1.00, 5.00)) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "RayTracing"="True" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float _MatType;
            float4 _Albedo;
            float _Roughness;
            float _IOR;
            float _Normal;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                //not even used
                return _Albedo;
            }
            ENDCG
        }
    }
}
