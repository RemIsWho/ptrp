﻿using Unity.Mathematics;

namespace Rem.Rendering.PathTracing
{
    [System.Serializable]
    public struct MeshObject // : IComponentData
    {
        public AABB Bounds;
        public AABB prevBounds;
        public float4x4 WorldToLocal;
        public float4x4 LocalToWorld;
        public float4x4 prevWorldToLocal;
        public float4x4 prevLocalToWorld;
        public int IndexStart;
        public int IndexLength;
        public int Material;
    }
}
