﻿using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;
using UnityEditor;

namespace Rem.Rendering
{
    public partial class CameraRenderer
    {
        public const int RT_ID = 1;
        public const int MV_ID = 10;
        public readonly RTRenderPipelineAsset asset;

        #region Constructor
        public CameraRenderer(RTRenderPipelineAsset asset)
        {
            this.asset = asset;
        }
        #endregion

        public void Render(RenderTargetIdentifier previousRT, RenderBuffers buffers, ScriptableRenderContext context, Camera camera)
        {
            this.context = context;
            this.camera = camera;

            PrepareBuffer();
            PrepareForSceneWindow();
            if (!Cull())
                return;

            Setup();
            SetupPT(buffers);
            DrawPT(previousRT);
            DrawGizmos();
            Submit();
        }
        void DrawPT(RenderTargetIdentifier previousRT)
        {
            cb.BeginSample("Path-Trace");
            int threadGroupsX = Mathf.CeilToInt(camera.scaledPixelWidth / 8f);
            int threadGroupsY = Mathf.CeilToInt(camera.scaledPixelHeight / 8f);

            //Motion Vector pass
            cb.GetTemporaryRT(MV_ID, camera.scaledPixelWidth, camera.scaledPixelHeight, 0, FilterMode.Point, RenderTextureFormat.RGFloat, RenderTextureReadWrite.Linear, 1, true);
            cb.SetComputeTextureParam(asset.RayTracingShader, 1, "MotionVectors", new RenderTargetIdentifier(MV_ID));
            cb.DispatchCompute(asset.RayTracingShader, 1, threadGroupsX, threadGroupsY, 1);

            //Main Color pass
            cb.GetTemporaryRT(RT_ID, camera.scaledPixelWidth, camera.scaledPixelHeight, 0, FilterMode.Point, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear, 1, true);
            cb.SetComputeTextureParam(asset.RayTracingShader, 0, "MotionVectors", new RenderTargetIdentifier(MV_ID));
            cb.SetComputeTextureParam(asset.RayTracingShader, 0, "Result", new RenderTargetIdentifier(RT_ID));
            cb.SetComputeTextureParam(asset.RayTracingShader, 0, "Previous", previousRT);
            cb.DispatchCompute(asset.RayTracingShader, 0, threadGroupsX, threadGroupsY, 1);
            cb.EndSample("Path-Trace");

            cb.BeginSample("Post-Processing");
            if (asset.UseAA) //TXAA with jittering and unjittering
            {
                cb.GetTemporaryRT(RT_ID+1, camera.scaledPixelWidth, camera.scaledPixelHeight, 0, FilterMode.Point, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear, 1, true);
                cb.Blit(new RenderTargetIdentifier(RT_ID), new RenderTargetIdentifier(RT_ID+1)); // copy
                cb.SetComputeTextureParam(asset.AntiAliasingShader, 0, "Result", new RenderTargetIdentifier(RT_ID+1));
                cb.SetComputeTextureParam(asset.AntiAliasingShader, 0, "Previous", previousRT);
                cb.DispatchCompute(asset.AntiAliasingShader, 0, threadGroupsX, threadGroupsY, 1);
                cb.ReleaseTemporaryRT(RT_ID+1);
            }

            //Update Previous
            cb.Blit(new RenderTargetIdentifier(RT_ID), previousRT); 

            //actually blit the screen with the final post-processing-effects
            if(asset.DebugStuffs == 0)
                cb.Blit(previousRT, BuiltinRenderTextureType.CameraTarget, asset._PostEffectMat);
            else if (asset.DebugStuffs == 1)
                cb.Blit(new RenderTargetIdentifier(MV_ID), BuiltinRenderTextureType.CameraTarget);

            cb.ReleaseTemporaryRT(MV_ID);
            cb.ReleaseTemporaryRT(RT_ID);
            cb.EndSample("Post-Processing");
        }

        #region Helper methods
        void SetupPT(RenderBuffers buffers)
        {
            //camera positions
            var jitter = new Vector2(Random.value, Random.value);
            jitter *= asset.MATRIX_JITTER;
            var pv = GetJitteredPerspectiveProjectionMatrix(camera, jitter);
            asset.RayTracingShader.SetMatrix("_oldVP", buffers.oldVP);
            asset.RayTracingShader.SetMatrix("_old_CameraToWorld", buffers.old_toWorld);
            asset.RayTracingShader.SetMatrix("_old_CameraInverseProjection", buffers.old_inverse);
            asset.RayTracingShader.SetMatrix("_CameraToWorld", camera.cameraToWorldMatrix);
            asset.RayTracingShader.SetMatrix("_CameraInverseProjection", pv.inverse);
#if UNITY_EDITOR
            asset.RayTracingShader.SetFloat("_Time", (float)EditorApplication.timeSinceStartup);
#else
            asset.RayTracingShader.SetFloat("_Time", Time.time);
#endif
            buffers.UpdateOldMatrix(pv, camera);
        }

        public static Matrix4x4 GetJitteredPerspectiveProjectionMatrix(Camera camera, Vector2 offset)
        {
            float vertical = Mathf.Tan(0.5f * Mathf.Deg2Rad * camera.fieldOfView) * camera.nearClipPlane;
            float horizontal = vertical * camera.aspect;

            offset.x *= horizontal / (0.5f * camera.scaledPixelWidth);
            offset.y *= vertical / (0.5f * camera.scaledPixelHeight); // should we unscale this width and height

            var matrix = camera.projectionMatrix;
            matrix[0, 2] += offset.x / horizontal;
            matrix[1, 2] += offset.y / vertical;
            return matrix;
        }
        #endregion
    }
}