﻿using Unity.Mathematics;

namespace Rem.Rendering.PathTracing
{
    [System.Serializable]
    public struct Sphere // : IComponentData
    {
        public float3 Pos;
        public float3 prevPos;
        public float Scale;
        public int Material;

        public Sphere(float x, float y, float z, float s, int mat)
        {
            Pos = new float3(x, y, z);
            prevPos = new float3(x, y, z);
            Scale = s;
            Material = mat;
        }
    }
}