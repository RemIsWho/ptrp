﻿using Unity.Mathematics;

namespace Rem.Rendering.PathTracing
{
    public enum LightType { Point = 0, Direction = 1, Area = 2 }

    [System.Serializable]
    public struct Light// : IComponentData
    {
        public uint Type;
        public float3 Pos; // motion vectors for prev pos and dir?
        public float3 prevPos;
        public float3 Dir;
        public float Scale;
        public float3 Color;

        public Light(float x, float y, float z, float s)
        {
            Type = (uint)LightType.Point;
            Pos = new float3(x, y, z);
            prevPos = new float3(x, y, z);
            Dir = float3.zero;
            Scale = s;
            Color = new float3(65.754f, 41.964f, 31.98f);
        }
    }
    public static class ColorUtils
    {
        public static float3 ColorToFloat3(UnityEngine.Color c)
        {
            return new float3(c.r, c.g, c.b);
        }
        public static UnityEngine.Color Float3ToColor(float3 f3)
        {
            return new UnityEngine.Color(f3.x, f3.y, f3.z);
        }
    }
}