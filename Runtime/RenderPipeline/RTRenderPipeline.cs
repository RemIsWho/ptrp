﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Rendering;
using Rem.Rendering.BVH;
using Unity.Mathematics;
using Rem.Rendering.PathTracing;
using UnityEngine.SceneManagement;

namespace Rem.Rendering
{
    public class RTRenderPipeline : RenderPipeline
    {
        #region Fields
        public const string AllowedShader = "PTRP/Standard";

        public readonly RTRenderPipelineAsset asset;
        readonly CameraRenderer renderer;
        RenderBuffers renderBuffers;
        RenderTexture defaultRT;
        BVH.Scene sceneBVH;
        bool updateTextures;
        bool sceneChanged = true;

        RenderTexture previewRT;
        RenderBuffers previewBuffers;
        //previous material
        UnityEngine.Material oldMat;
        bool PreviousLight;
        #endregion

        #region Constructor
        public RTRenderPipeline(RTRenderPipelineAsset asset)
        {
            this.asset = asset;
            renderer = new CameraRenderer(asset);

            //sceneBVH = new BVH.Scene(this);
            renderBuffers = new RenderBuffers();
            previewBuffers = new RenderBuffers();
            listRGBTextures = new List<Texture2D>();
            listRTextures = new List<Texture2D>();
        }
        #endregion

        #region Main Methods
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (previewRT != null)
                    previewRT.Release();
                if (defaultRT != null)
                    defaultRT.Release();
                if (sceneBVH != null)
                    sceneBVH.Dispose();

                renderBuffers.Dispose();
                previewBuffers.Dispose();

                listRGBTextures.Clear();
                listRGBTextures = null;
                listRTextures.Clear();
                listRTextures = null;
            }
        }
        protected override void Render(ScriptableRenderContext context, Camera[] cameras)
        {
            foreach (var cam in cameras)
            {
                #region Preview
                if (EditorSceneManager.IsPreviewScene(cam.scene) && cam.cameraType == CameraType.Preview)
                {
                    var objects = cam.scene.GetRootGameObjects();
                    bool twolights = objects[2].GetComponent<UnityEngine.Light>().intensity > 0f;
                    if (Selection.activeObject != null && Selection.activeObject.GetType() != null)
                    {
                        switch (Selection.activeObject.GetType().Name)
                        {
                            case "Material":
                                //only draw that material on a sphere
                                UnityEngine.Material mat = Selection.activeObject as UnityEngine.Material;
                                RenderPreview(mat, context, cam, twolights);
                                continue;

                            case "GameObject":
                                GameObject go = Selection.activeObject as GameObject;
                                var render = go.GetComponent<Renderer>();
                                if (render != null)
                                {
                                    RenderPreview(render.sharedMaterial, context, cam, twolights);
                                }
                                continue;
                        }
                    }
                    else
                    {
                        Debug.LogWarning("preview without selection, not added yet. Fallback ->");
                    }
                }
                #endregion

                #region Game and Editor 
                else if (cam.cameraType != CameraType.Preview)
                {
                    if(asset.ECS_ONLY_INGAME && Application.isPlaying)
                    {
                        //use ECS stuffs
                        //should be much faster than any other option, we start by grabbing the main System
                        //and get all the stuff for the renderBuffers and set it to the shader
                        // ECSrenderBuffers.PushToShader(asset.RayTracingShader);
                    }
                    else if (cam.cameraType == CameraType.SceneView && SceneView.currentDrawingSceneView.sceneLighting || cam.cameraType == CameraType.Game)
                    {
                        //setup default scene
                        if (sceneChanged || asset.ForceUpdateScene)
                        {
                            if (sceneBVH != null)
                                sceneBVH.Dispose();

                            sceneBVH = new BVH.Scene(this);
                            sceneBVH.SetupScene(EditorSceneManager.GetActiveScene());
                        }

                        UpdateAndPushTextures(asset.RayTracingShader);
                        renderBuffers.SetBuffers(sceneBVH);
                        if (!renderBuffers.isReady)
                            goto fallback;

                        renderBuffers.PushToShader(asset.RayTracingShader);
                        InitRenderTexture(ref defaultRT, cam, RenderTextureFormat.ARGBFloat, false);
                        renderer.Render(new RenderTargetIdentifier(defaultRT), renderBuffers, context, cam);

                        //set previous matrixes
                        //sceneBVH.UpdatePrev(EditorSceneManager.GetActiveScene());
                        sceneChanged = false;
                        return;
                    }
                }
                #endregion

                fallback:;
                renderer.Render(context, cam); //fallback, also to display errors
            }
        }
        #endregion

        #region Helper methods
        public void RenderPreview(UnityEngine.Material mat, ScriptableRenderContext context, Camera cam, bool twoLights)
        {
            if (mat.shader.name != AllowedShader)
            {
                renderer.Render(context, cam);
                return; // basically error texture!
            }

            if (cam.scaledPixelHeight == 64 && cam.scaledPixelWidth == 64)
            {
                //just copies the preview over to the small icon, since this is what we have when we have the icon
                renderer.RenderCopy(new RenderTargetIdentifier(previewRT), context, cam);
            }
            else
            {
                //just create a temporary bvh, just to override the buffers
                //because we store the main bvh, so we don't need to destroy it
                //also makes it easier doing custom stuff
                //there is also no need to keep this bvh structure, because we don't want to create motion vectors or anything ^^
                using (BVH.Scene bvh = new BVH.Scene(this))
                {
                    bvh.Add(new PathTracing.Sphere(0f, 0f, 0f, 0.4f, 0), mat, 0);
                    if (twoLights)
                    {
                        bvh.Add(new PathTracing.Light(2f, 2f, 0f, 0.4f) { Color = new float3(0f, 1f, 0.23f) * 14f }, 1);
                        bvh.Add(new PathTracing.Light(-2f, 2f, 2f, 0.4f) { Color = new float3(1f, 0.178f, 0f) * 14f }, 2);
                    }
                    else
                    {
                        bvh.Add(new PathTracing.Light(2f, 2f, 0f, 0.4f), 1);
                    }
                    previewBuffers.SetBuffers(bvh);
                }

                UpdateAndPushTextures(asset.RayTracingShader);
                previewBuffers.PushToShader(asset.RayTracingShader);

                InitRenderTexture(ref previewRT, cam, RenderTextureFormat.ARGBFloat, oldMat != mat || PreviousLight != twoLights);
                renderer.Render(new RenderTargetIdentifier(previewRT), previewBuffers, context, cam);
                oldMat = mat;
                PreviousLight = twoLights;
            }
        }
        public static void InitRenderTexture(ref RenderTexture rt, Camera camera, RenderTextureFormat format, bool forceRelease = false)
        {
            if (rt == null || rt.width != camera.scaledPixelWidth || rt.height != camera.scaledPixelHeight || forceRelease)
            {
                // Release render texture if we already have one
                if (rt != null)
                {
                    rt.Release();
                }

                // Get a render target for Ray Tracing
                rt = new RenderTexture(camera.scaledPixelWidth, camera.scaledPixelHeight, 0, format, RenderTextureReadWrite.Linear)
                {
                    enableRandomWrite = true,
                    filterMode = FilterMode.Bilinear,
                    antiAliasing = 1 // off
                };
                rt.Create();
            }
        }
        #endregion

        #region Textures
        List<Texture2D> listRGBTextures;
        List<Texture2D> listRTextures;

        Texture2DArray RGBTextures;
        Texture2DArray RTextures;
        public PathTracing.Material GetMat(UnityEngine.Material mat)
        {
            return new PathTracing.Material()
            {
                Type = (uint)mat.GetFloat("_MatType"),
                Albedo = ColorUtils.ColorToFloat3(mat.GetColor("_Albedo")),
                Roughness = mat.GetFloat("_Roughness"),
                IOR = mat.GetFloat("_IOR"),
                Normal = mat.GetFloat("_Normal"),
                TextureScaling = mat.mainTextureScale,
                TextureID = UpdateTextureList((Texture2D)mat.GetTexture("_MainTex"), ref listRGBTextures),
                NormalTextureID = UpdateTextureList((Texture2D)mat.GetTexture("_NormalTex"), ref listRGBTextures),
                MetalTextureID = UpdateTextureList((Texture2D)mat.GetTexture("_MetalTex"), ref listRTextures)
            };
        }
        public void UpdateAndPushTextures(ComputeShader shader)
        {
            if (!updateTextures)
                return;

            RGBTextures = new Texture2DArray(listRGBTextures[0].width, listRGBTextures[0].height, listRGBTextures.Count, listRGBTextures[0].format, true);
            for (int i = 0; i < listRGBTextures.Count; i++)
                Graphics.CopyTexture(listRGBTextures[i], 0, RGBTextures, i);

            RTextures = new Texture2DArray(listRTextures[0].width, listRTextures[0].height, listRTextures.Count, listRTextures[0].format, true);
            for (int i = 0; i < listRTextures.Count; i++)
                Graphics.CopyTexture(listRTextures[i], 0, RTextures, i);

            if (RGBTextures != null)
                shader.SetTexture(0, "_RGBTextures", RGBTextures);
            if (RTextures != null)
            {
                shader.SetTexture(0, "_RTextures", RTextures);
                shader.SetTexture(1, "_RTextures", RTextures);
            }

            updateTextures = false;
        }
        public int UpdateTextureList(Texture2D texture, ref List<Texture2D> textures)
        {
            if (texture == null)
                return -1;

            int index = textures.IndexOf(texture);
            if (index != -1)
                return index;

            textures.Add(texture);
            updateTextures = true;
            return textures.Count - 1;
        }
        #endregion
    }
}