﻿using Unity.Mathematics;

namespace Rem.Rendering.PathTracing
{
    public enum MatType : uint { Lambert = 0, Specular = 1, Dielectric = 2, Emissive = 3, Volume = 4 }

    [System.Serializable]
    public struct Material
    {
        public uint Type;
        public float3 Albedo;
        public float Roughness;
        public float IOR;
        public float Normal;
        public float2 TextureScaling;
        /*Textures*/
        public int TextureID;
        public int NormalTextureID;
        public int MetalTextureID;

        public Material(float x, float y, float z, MatType type, int textureID = -1, int normalTextureID = -1, int metalTextureID = -1)
        {
            Type = (uint)type;
            Albedo = new float3(x, y, z);
            Roughness = .0f;
            IOR = 1.0f;
            Normal = 0f;
            TextureScaling = float2.zero;
            TextureID = textureID;
            NormalTextureID = normalTextureID;
            MetalTextureID = metalTextureID;
        }
    }
}