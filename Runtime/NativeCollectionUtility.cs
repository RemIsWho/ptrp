﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Rem.Common
{
    public static class NativeCollectionUtility
    {
        #region NativeArray as NativeList
        public unsafe static void Add<T>(this NativeArray<T> me, ref int count, T value, Allocator allocator = Allocator.Persistent) where T : struct
        {
            if (count == me.Length)
            {
                me.Resize(count * 2, allocator);
            }
            me[count] = value;
            count++;
        }

        public unsafe static void Resize<T>(this NativeArray<T> me, int capacity, Allocator allocator = Allocator.Persistent) where T : struct
        {
            NativeArray<T> newArray = new NativeArray<T>(capacity, allocator);
            if (capacity > me.Length)
            {
                UnsafeUtility.MemCpy(
                    newArray.GetUnsafePtr(),
                    me.GetUnsafeReadOnlyPtr(),
                    me.Length * UnsafeUtility.SizeOf<T>());
            }
            else
            {
                UnsafeUtility.MemCpy(
                    newArray.GetUnsafePtr(),
                    me.GetUnsafeReadOnlyPtr(),
                    me.Length * UnsafeUtility.SizeOf<T>());
            }
            me.Dispose();
            me = newArray;
        }
        #endregion

        #region Get pointer with index
        public unsafe static void* GetUnsafePtrWithIndex<T>(this NativeArray<T> me, int index = 0) where T : struct
        {
            int elementSize = UnsafeUtility.SizeOf<T>();
            void* buffer = me.GetUnsafePtr();
            return (byte*)buffer + (elementSize * index);
        }
        public unsafe static void* GetUnsafePtrWithIndex<T>(this NativeList<T> me, int index = 0) where T : struct
        {
            int elementSize = UnsafeUtility.SizeOf<T>();
            void* buffer = me.GetUnsafePtr();
            return (byte*)buffer + (elementSize * index);
        }
        public unsafe static void MemCpy(void* destination, int destinationOffset, void* source, int sourceOffset, int elementSize, long size)
        {
            destination = (byte*)destination + (elementSize * destinationOffset);
            source = (byte*)source + (elementSize * sourceOffset);
            UnsafeUtility.MemCpy(destination, source, size * elementSize);
        }


        public unsafe static void MemCpySingleToMultiple(void* destination, int elementsize, void* source, byte numberOfParts, int elementCount, byte part)
        {
            int partSize = elementsize / numberOfParts;

            UnsafeUtility.MemCpyStride(
                (byte*)destination + (partSize * part),
                elementsize,
                source,
                partSize,
                partSize,
                elementCount
            );
        }
        #endregion

        #region Array Pointers
        public unsafe static void* GetUnsafePointer<T>(this T[] me, int index = 0) where T : unmanaged
        {
            if (index < 0 || index >= me.Length)
                throw new ArgumentOutOfRangeException($"Index {index} out of range {me.Length}, can't get pointer", nameof(index));

            fixed (void* element = &me[index])
                return element;
        }
        public unsafe static void* GetUnsafePointer<T>(this T[,] me, int firstIndex = 0, int secondIndex = 0) where T : unmanaged
        {
            if (firstIndex < 0 || firstIndex >= me.GetLength(0))
                throw new ArgumentOutOfRangeException($"Row index {firstIndex} out of range {me.GetLength(0)}, can't get pointer", nameof(firstIndex));
            if (secondIndex < 0 || secondIndex >= me.GetLength(1))
                throw new ArgumentOutOfRangeException($"Row index {secondIndex} out of range {me.GetLength(1)}, can't get pointer", nameof(secondIndex));

            fixed (void* element = &me[firstIndex, secondIndex])
                return element;
        }
        #endregion

        #region Reinterpret
        public unsafe static NativeArray<T> AsNativeArray<T>(void* pointer, int size) where T : struct
        {
            var nativeArray = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<T>(pointer, size, Allocator.Invalid);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
            NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref nativeArray, AtomicSafetyHandle.Create());
#endif
            return nativeArray;
        }
        public static unsafe NativeArray<TO> Reinterpret<TI, TO>(this NativeArray<TI> nativeArray)
    where TI : struct
    where TO : struct
        {
            return AsNativeArray<TO>(nativeArray.GetUnsafePtr(), nativeArray.Length);
        }
        public unsafe static NativeArray<byte> AsNativeArray(void* pointer, int size)
        {
            var nativeArray = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<byte>(pointer, size, Allocator.Invalid);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
            NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref nativeArray, AtomicSafetyHandle.Create());
#endif
            return nativeArray;
        }
        public unsafe static NativeList<T> Init<T>(NativeList<T> source, Allocator allocator = Allocator.Persistent) where T : struct
        {
            NativeList<T> list = new NativeList<T>(source.Capacity, allocator);
            int elementSize = UnsafeUtility.SizeOf<T>();
            UnsafeUtility.MemCpy(list.GetUnsafePtr(), source.GetUnsafePtr(), source.Length * elementSize);

            return list;
        }
        #endregion

        #region Clear and SetAll
        public unsafe static void Clear<T>(this NativeArray<T> me) where T : struct
        {
            UnsafeUtility.MemClear(me.GetUnsafePtr(), me.Length * UnsafeUtility.SizeOf<T>());
        }

        public unsafe static void SetAll<T>(this NativeArray<T> me, T value) where T : unmanaged
        {
            SetAll(me.GetUnsafePtr(), me.Length, value);
        }

        public unsafe static void SetAll<T>(void* dst, int length, T value) where T : unmanaged
        {
            T[] ts = new T[1] { value };
            int size = UnsafeUtility.SizeOf<T>();
            UnsafeUtility.MemCpyStride(dst, size, ts.GetUnsafePointer(), 0, size, length);
        }
        #endregion

        public static void Reverse<T>(this NativeList<T> list)
            where T : struct
        {
            var length = list.Length;
            var index1 = 0;

            for (var index2 = length - 1; index1 < index2; --index2)
            {
                var obj = list[index1];
                list[index1] = list[index2];
                list[index2] = obj;
                ++index1;
            }
        }

        public static bool Remove<T, TI>(this NativeList<T> list, TI element)
            where T : struct, IEquatable<TI>
            where TI : struct
        {
            var index = list.IndexOf(element);
            if (index < 0)
            {
                return false;
            }

            list.RemoveAt(index);
            return true;
        }
        public static void RemoveAt<T>(this NativeList<T> list, int index)
            where T : struct
        {
            list.RemoveRangeWithBeginEnd(index, 1);
        }

        public static unsafe void RemoveRange<T>(this NativeList<T> list, int index, int count)
            where T : struct
        {
#if ENABLE_UNITY_COLLECTIONS_CHECKS
            if ((uint)index >= (uint)list.Length)
            {
                throw new IndexOutOfRangeException(
                    $"Index {index} is out of range in NativeList of '{list.Length}' Length.");
            }
#endif

            int elemSize = UnsafeUtility.SizeOf<T>();
            byte* basePtr = (byte*)list.GetUnsafePtr();

            UnsafeUtility.MemMove(
                basePtr + (index * elemSize),
                basePtr + ((index + count) * elemSize),
                elemSize * (list.Length - count - index)
            );

            // No easy way to change length so we just loop this unfortunately.
            for (var i = 0; i < count; i++)
            {
                list.RemoveAtSwapBack(list.Length - 1);
            }
        }
        public static void AddRange<T>(this NativeList<T> me, params T[] elements) where T : struct
        {
            for (int i = 0; i < elements.Length; i++)
            {
                me.Add(elements[i]);
            }
        }
    }
}
