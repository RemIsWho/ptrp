﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Rendering;

namespace Rem.Rendering
{
    public partial class CameraRenderer
    {
        #region Fields
        const string bufferName = "Render Camera";
        CommandBuffer cb = new CommandBuffer { name = bufferName };
        ScriptableRenderContext context;
        Camera camera;
        #endregion

        CullingResults cullingResults;
        static ShaderTagId ptShaderTagId = new ShaderTagId("SRPDefaultUnlit");
        public void Render(ScriptableRenderContext context, Camera camera)
        {
            this.context = context;
            this.camera = camera;

            PrepareBuffer();
            PrepareForSceneWindow();
            if (!Cull())
                return;

            Setup();
            DrawRegularGeometry();
            DrawUnsupportedShaders();
            DrawGizmos();
            Submit();
        }
        public void RenderCopy(RenderTargetIdentifier image, ScriptableRenderContext context, Camera camera)
        {
            this.context = context;
            this.camera = camera;

            PrepareBuffer();
            PrepareForSceneWindow();
            if (!Cull())
                return;

            Setup();
            cb.Blit(image, BuiltinRenderTextureType.CameraTarget);
            Submit();
        }

        #region Regular rendering
        bool Cull()
        {
            if (camera.TryGetCullingParameters(out ScriptableCullingParameters p))
            {
                cullingResults = context.Cull(ref p);
                return true;
            }
            return false;
        }
        void DrawRegularGeometry()
        {
            var sortingSettings = new SortingSettings(camera);
            var drawingSettings = new DrawingSettings(ptShaderTagId, sortingSettings);
            var filteringSettings = new FilteringSettings(RenderQueueRange.opaque);

            context.DrawRenderers(
                cullingResults, ref drawingSettings, ref filteringSettings
            );

            context.DrawSkybox(camera);

            sortingSettings.criteria = SortingCriteria.CommonTransparent;
            drawingSettings.sortingSettings = sortingSettings;
            filteringSettings.renderQueueRange = RenderQueueRange.transparent;

            context.DrawRenderers(
                cullingResults, ref drawingSettings, ref filteringSettings
            );
        }
        #endregion

        #region Helper Methods
        void Setup()
        {
            context.SetupCameraProperties(camera);
            CameraClearFlags flags = camera.clearFlags;
            cb.ClearRenderTarget(
                flags <= CameraClearFlags.Depth,
                flags == CameraClearFlags.Color,
                flags == CameraClearFlags.Color ?
                    camera.backgroundColor.linear : Color.clear
            );
            cb.BeginSample(SampleName);
            ExecuteBuffer();
        }
        void Submit()
        {
            cb.EndSample(SampleName);
            ExecuteBuffer();
            context.Submit();
        }
        void ExecuteBuffer()
        {
            context.ExecuteCommandBuffer(cb);
            cb.Clear();
        }
        #endregion
    }
}