# Custom Path Traced RenderPipeline

Supports the following materials: Lambert, Specular, dielectric, Emissive.

Works with any mesh, more triangles = worse performance. Has special primitive for perfect spheres (since they can be calculated much faster than any triangle intersection).

- All the path tracing is done in a single compute shader
- Uses skybox
- No denoising just importance sampling + temporal accumulation (with reprojection) + TXAA
- Post processing effects (Chromatic Aberration, ACES tonemapping, vignette)

## Showcase

> note that these gifs where recorded using 30fps and then compressed to .gif format (to fit the discord file size limit)

![](./docs/AlmostPerfectReflections2.gif)

![](./docs/FixedMeshTriangleMovementBug.gif)

![](./docs/MotionVectors_At_Last2_2.gif)

## installation / usage

1. Install the package
2. Make a new `RTRenderPipelineAsset` and use it instead of the default renderpipeline
3. update all materials to use the `PTRP/Standard` Shader