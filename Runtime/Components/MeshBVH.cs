﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;

[RequireComponent(typeof(MeshFilter))]
public class MeshBVH : MonoBehaviour
{
    #region Fields
    public static int MaxFace = 30; //no more than 30 triangles per bvh node

    private MeshFilter mf;
    private int processID;
    #endregion

    #region Unity Events
    private void Awake()
    {
        mf = GetComponent<MeshFilter>();
    }
    #endregion

    #region Build BVH
    public void Process()
    {
        if (mf.sharedMesh.GetInstanceID() == processID)
            return; // already done

        processID = mf.sharedMesh.GetInstanceID();
        //build BVH for mesh

        //NativeArray<int> trils = mf.sharedMesh.vertices;
    }
    #endregion
}
