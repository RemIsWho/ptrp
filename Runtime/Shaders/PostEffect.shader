﻿Shader "Hidden/PostEffect"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Exposure("Exposure", Range(0.1, 3.)) = 1
        _VignettePower("Vignette Power", Range(0., 8.)) = 1.1
        _AberrationOffset("Aberration Offset", Range(0., 4.)) = .3
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always Blend Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            uniform float _Exposure;
            uniform float _VignettePower;
            uniform float _AberrationOffset;

            float3 ACESFilm2020(float3 x)
            {
                float a = 15.8f;
                float b = 2.12f;
                float c = 1.2f;
                float d = 5.92f;
                float e = 1.9f;
                return (x * (a * x + b)) / (x * (c * x + d) + e);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                //grabing the result from the RT pipline
                float4 col = 0.;
                float2 coords = i.uv.xy;

                //Chromatic Aberration
                _AberrationOffset /= 300.;
                float4 red = tex2D(_MainTex, coords.xy - _AberrationOffset);
                float4 green = tex2D(_MainTex, coords.xy);
                float4 blue = tex2D(_MainTex, coords.xy + _AberrationOffset);
                col.rgb = float3(red.r, green.g, blue.b) * _Exposure;

                // ACES tonemapping/colorgrading
                col = pow(col, 1.25); 
                col.rgb = ACESFilm2020(col.rgb);

                //vignette
                float2 dist = (i.uv - .5) * 1.25;
                dist.x = 1. - dot(dist, dist) * _VignettePower;
                col *= dist.x;
                return col;
            }
            ENDCG
        }
    }
}
