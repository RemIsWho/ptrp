﻿#pragma kernel RTColor
#pragma kernel RTMotion

#include "random.hlsl"
#include "maths.hlsl"

#define MaxDist 1e20
#define eps 0.001 // epsilon
#define kCSGroupSizeX 8
#define kCSGroupSizeY 8

int SAMPLES_PER_PIXEL; // we only need 1 sample per pixel because we have TRAA
int BOUNCES_PER_SAMPLE; //this is the number of rays one sample can make (AO, relfection)
int BOUNCES_WITH_LIGHT; //this value must be less than the Bounces_PER_SAMPLE (Shadows, Lights)
int REPROJ_MAXFRAMES; // max frames the raytracer will use before the image will change

RWTexture2D<float2> MotionVectors;
RWTexture2D<float4> Result;
Texture2D<float4> Previous;
SamplerState sampler_Previous;

float4x4 _oldVP;
float4x4 _old_CameraToWorld;
float4x4 _old_CameraInverseProjection;
float4x4 _CameraToWorld;
float4x4 _CameraInverseProjection;

float _Time;
float _DistanceLOD;

Texture2D<float> _BlueNoise;
SamplerState sampler_BlueNoise;

Texture2D<float4> _SkyboxTexture;
SamplerState sampler_SkyboxTexture;

Texture2DArray<float> _RTextures;
SamplerState sampler_RTextures;
Texture2DArray<float3> _RGBTextures;
SamplerState sampler_RGBTextures;

// STRUCTURE
StructuredBuffer<Material> _Materials;
StructuredBuffer<Light> _Lights;
StructuredBuffer<Sphere> _Spheres;
//StructuredBuffer<BVHData> _BVH;

StructuredBuffer<MeshObject> _Meshes;
StructuredBuffer<float3> _Vertexes;
StructuredBuffer<float3> _Normals;
StructuredBuffer<float2> _UVs;
StructuredBuffer<int> _Indices;

//-----------------------------
//- RAY
Ray CreateCameraRay(in float2 uv, in float4x4 toWorld, in float4x4 inverseVP)
{
	// Transform the camera origin to world space
    float3 origin = mul(toWorld, float4(0., 0., 0., 1.)).xyz;
	// Invert the perspective projection of the view-space position
    float3 direction = mul(inverseVP, float4(uv, 0., 1.)).xyz;
	// Transform the direction from camera to world space and normalize
    direction = normalize(mul(toWorld, float4(direction, 0.)).xyz);
    
    //DoF - real camera simulation
    /*float3 fp = origin + direction * fpd;
    origin += mul(_CameraInverseProjection, float4(randPentagon(), 0., 0.)).xyz * fpp;
    direction = normalize(fp - origin);*/
    
    return CreateRay(origin, direction);
}

//-----------------------------
//- TODO
//then start working on a chunk system, and bounding box system and add meshes
//https://www.shadertoy.com/view/Wt3XRX <- SDF with reprojection (bloom + DoF)
//https://www.shadertoy.com/view/wtcXz4 <- SDF --- / / ----
//https://www.shadertoy.com/view/4ds3zr <- voxel grid RT
//https://www.shadertoy.com/view/tdlSR8 <- aboslute crazy voxel RT

//https://www.youtube.com/watch?v=2XXS5UyNjjU <- Temporal reprojection AA in INSIDE
//https://github.com/playdeadgames/temporal/blob/master/Assets/Shaders/TemporalReprojection.shader
//https://raytracing.github.io/books/RayTracingInOneWeekend.html#wherenext? <- BVH tree
//http://lup.lub.lu.se/luur/download?func=downloadFile&recordOId=8971248&fileOId=8971249
//https://kennyalive.com/post/math-typesetting-2020/
//https://miketuritzin.com/post/rendering-particles-with-compute-shaders/ <- GPU particles

//-----------------------------
//- Blue noise
float bluenoise(in float2 uv)
{
    return _BlueNoise.SampleLevel(sampler_BlueNoise, uv, 0.0);
}

//------------------------------
//- INTERSECTION
float3 nSphere(in float3 pos, in float4 sph)
{
    return (pos - sph.xyz) / sph.w;
}
float iSphere(in float3 ro, in float3 rd, in float4 sph)
{
    float3 oc = ro - sph.xyz;
    float b = dot(oc, rd);
    float c = dot(oc, oc) - sph.w * sph.w;
    float h = b * b - c;
    if (h < 0.0)
        return -1.0;

    float s = sqrt(h);
    float t1 = -b - s;
    float t2 = -b + s;
	
    return t1 < 0.0 ? t2 : t1;
}

float intersectMesh(float3 ro, float3 rd, MeshObject mesh, float4x4 worldToLocal, float4x4 localToWorld, out float3 normal, out float2 HitUV)
{
    float t = MaxDist;
    normal = 0.;
    HitUV = 0.;
    ro = mul(worldToLocal, float4(ro, 1.)).xyz;
    rd = mul((float3x3) worldToLocal, rd);
    
    uint start = mesh.indexStart;
    uint length = start + mesh.indexLength;
    float3 v0, v1, v2;
    uint it = start;
    float2 uv;
    for (uint i = start; i < length; i += 3)
    {
        v0 = _Vertexes[_Indices[i]];
        v1 = _Vertexes[_Indices[i + 1]];
        v2 = _Vertexes[_Indices[i + 2]];

        float t1, u, v;
        if (IntersectTriangle_MT97(ro, rd, v0, v1, v2, t1, u, v))
        {
            if (t1 > eps && t1 < t)
            {
                t = t1;
                it = i;
                uv = float2(u, v);
            }
        }
    }
    
    if (t < MaxDist)
    {
        //only calculate the normal once, for the mesh
        normal = lerp(lerp(_Normals[_Indices[it]], _Normals[_Indices[it + 1]], uv.x), _Normals[_Indices[it + 2]], uv.y);
        normal = normalize(mul((float3x3) localToWorld, normal));
        HitUV = lerp(lerp(_UVs[_Indices[it]], _UVs[_Indices[it + 1]], uv.x), _UVs[_Indices[it + 2]], uv.y);
    }
    return t;
}

//----------------------------------------
//- SCENE
RayHit intersect(in Ray r, inout float3 normal)
{
    float t;
    uint count, stride;
    RayHit hit = CreateHit(MaxDist, -1);
    
    _Lights.GetDimensions(count, stride);
    for (uint l = 0; l < count; l++)
    {
        if (_Lights[l].type == LightPoint)
        {
            t = iSphere(r.origin, r.direction, float4(_Lights[l].pos, _Lights[l].scale));
            if (t > eps && t < hit.distance)
            {
                hit.distance = t;
                hit.material = 1000 + l;
                normal = nSphere(r.origin + t * r.direction, float4(_Lights[l].pos, _Lights[l].scale));
            }
        }
        else if (_Lights[l].type == LightArea)
        {
            t = IntersectDisk(r.origin, r.direction, _Lights[l].pos, -_Lights[l].dir, _Lights[l].scale);
            if (t > eps && t < hit.distance)
            {
                hit.distance = t;
                hit.material = 1000 + l;
                normal = _Lights[l].dir;
            }
        }
    }
    
    _Spheres.GetDimensions(count, stride);
    for (uint s = 0; s < count; s++)
    {
        t = iSphere(r.origin, r.direction, float4(_Spheres[s].pos, _Spheres[s].scale));
        if (t > eps && t < hit.distance)
        {
            hit.distance = t;
            hit.material = _Spheres[s].mat;
            normal = nSphere(r.origin + t * r.direction, float4(_Spheres[s].pos, _Spheres[s].scale));
            hit.hasUV = true;
            hit.uv = float2(acos(normal.y) / -PI, atan2(normal.x, -normal.z) / -PI * 0.5f);
        }
    }
    
    float3 tmpNorm;
    float2 tmpUV;
    _Meshes.GetDimensions(count, stride);
    for (uint m = 0; m < count; m++)
    {
        if (Intersect(r.origin, rcp(r.direction), _Meshes[m].bounds, eps, hit.distance))
        {
            t = intersectMesh(r.origin, r.direction, _Meshes[m], _Meshes[m].worldToLocal, _Meshes[m].localToWorld, tmpNorm, tmpUV);
            if (t > eps && t < hit.distance)
            {
                hit.distance = t;
                hit.uv = tmpUV;
                hit.hasUV = true;
                hit.material = _Meshes[m].mat;
                normal = tmpNorm;
            }
        }
    }
    return hit;
}

bool intersectShadow(in float3 ro, in float3 rd, in float dist)
{
    float t;
    uint count, stride;
    _Spheres.GetDimensions(count, stride);
    for (uint s = 0; s < count; s++)
    {
        t = iSphere(ro, rd, float4(_Spheres[s].pos, _Spheres[s].scale));
        if (t > eps && t < dist)
        {
            return true;
        }
    }
    float3 tmpNorm;
    float2 tmpUV;
    _Meshes.GetDimensions(count, stride);
    for (uint m = 0; m < count; m++)
    {
        if (Intersect(ro, rcp(rd), _Meshes[m].bounds, eps, dist))
        {
            t = intersectMesh(ro, rd, _Meshes[m], _Meshes[m].worldToLocal, _Meshes[m].localToWorld, tmpNorm, tmpUV);
            if (t > eps && t < dist)
            {
                return true;
            }
        }
    }
    return false;
}

//--------------------------------
//- LIGHT
float3 sampleLight(inout float seed, Light light)
{
    if (light.type == LightPoint)
    {
        float3 n = randomSphereDirection(seed) * light.scale;
        return light.pos + n;
    }
    
    return light.pos + cosWeightedRandomHemisphereDirection(light.dir, seed) * light.scale;
}
void SampleLights(const in float3 ro, const in float3 normal, in uint lightCount, in float3 fcol, inout float seed, inout float3 tcol)
{
    for (uint l = 0; l < lightCount; l++)
    {
        float3 ld = sampleLight(seed, _Lights[l]) - ro;
        float3 nld = normalize(ld);
        if (!intersectShadow(ro, nld, length(ld)))
        {
            if (_Lights[l].type == LightPoint)
            {
                float cos_a_max = sqrt(1. - clamp(_Lights[l].scale * _Lights[l].scale / dot(_Lights[l].pos - ro, _Lights[l].pos - ro), 0., 1.));
                float weight = 2. * (1. - cos_a_max);
                tcol += (fcol * _Lights[l].col) * (weight * clamp(dot(nld, normal), 0., 1.));
            }
            else
            {
                tcol += (fcol * _Lights[l].col) * clamp(dot(nld, -_Lights[l].dir), 0., 1.);
            }
        }
    }
}

//------------------------------------------
//- MATERIALS
float3 matColor(const in RayHit hit, in int textureID)
{
    if (hit.hasUV && textureID != -1)
        return _RGBTextures.SampleLevel(sampler_RGBTextures, float3(hit.uv * _Materials[hit.material].scaling, textureID), hit.distance * _DistanceLOD) * _Materials[hit.material].albedo;
    else
        return _Materials[hit.material].albedo;
}
void NormalTexture(inout float3 normal, in RayHit hit, in int normalID)
{
    float3 tmpNorm = matColor(hit, normalID); //(matColor(hit, 1) * 2.) - 1.;

    float3 tangent;
    float3 c1 = cross(normal, float3(0., 0., 1.));
    float3 c2 = cross(normal, float3(0., 1., 0.));
    if (length(c1) < length(c2))
        tangent = c1;
    else
        tangent = c2;
            
    float3 bitangent = cross(tangent, normal);
    normal = lerp(normal, normalize((tmpNorm.x * tangent) + (tmpNorm.y * bitangent) + (tmpNorm.z * normal)), _Materials[hit.material].normal);
}
//-------------------------------------------
//- BRDF
float3 getBRDFRay(in float3 n, const in float3 rd, const in RayHit hit, inout bool specularBounce, inout float seed)
{
    specularBounce = false;
    
    float3 r = cosWeightedRandomHemisphereDirection(n, seed);
    Material mat = _Materials[hit.material];
    if (mat.type == MatLambert)
    {
        return r;
    }
    else
    {
        specularBounce = true;
        float n1, n2, ndotr = dot(rd, n);
        
        if (ndotr > 0.)
        {
            n1 = 1.;
            n2 = mat.IOR;
            n = -n;
        }
        else
        {
            n1 = mat.IOR;
            n2 = 1.;
        }
                
        float r0 = (n1 - n2) / (n1 + n2);
        r0 *= r0;
        float fresnel = r0 + (1. - r0) * pow(1. - abs(ndotr), 2.);
        
        float3 ref;
        if (hash1(seed) < fresnel || mat.type == MatSpecular)
            ref = reflect(rd, n);
        else
            ref = refract(rd, n, n2 / n1);
        
        float roughness = mat.roughness;
        if (hit.hasUV && mat.MetalTextureID != -1)
            roughness = roughness * (1. - _RTextures.SampleLevel(sampler_RTextures, float3(hit.uv * mat.scaling, mat.MetalTextureID), hit.distance * _DistanceLOD));
        
        return normalize(ref + roughness * r);
    }
}


//--------------------------------------------
//- TRACING
float3 Trace(in uint lightCount, in Ray r, inout float seed)
{
    float3 tcol = 0.;
    float3 fcol = 1.;
    float3 normal;
    bool specularBounce = true;
    for (int j = 0; j < BOUNCES_PER_SAMPLE; j++)
    {
        RayHit hit = intersect(r, normal);
        
        if (hit.material < 0)
        {
            float theta = acos(r.direction.y) / -PI;
            float phi = atan2(r.direction.x, -r.direction.z) / -PI * .5;
            return tcol + fcol * _SkyboxTexture.SampleLevel(sampler_SkyboxTexture, float2(phi, theta), 0.).xyz;
        }
        else if (hit.material >= 1000)
        {
            if (specularBounce) //lights have material 1000+...
                tcol += fcol * _Lights[hit.material - 1000].col;
            return tcol;
        }
        else if (_Materials[hit.material].type == MatEmissive)
        {
            return tcol + fcol * matColor(hit, _Materials[hit.material].textureID); //Emissive materials
        }
        
        int normalID = _Materials[hit.material].NormalTextureID;
        if (hit.hasUV && normalID != -1) //if has uvs, then update the normal base on the normal map
        {
            NormalTexture(normal, hit, normalID);
        }
        
        r.origin += hit.distance * r.direction;
        r.direction = getBRDFRay(normal, r.direction, hit, specularBounce, seed);
        
        fcol *= matColor(hit, _Materials[hit.material].textureID);
        
        //shadows / lights
        if (!specularBounce && j < BOUNCES_WITH_LIGHT)
        {
            SampleLights(r.origin, normal, lightCount, fcol, seed, tcol);
        }
    }
    return tcol;
}

//-------------------------------------------------------------------
//- MOTION VECTORS
float MotionMesh(float3 ro, float3 rd, MeshObject mesh, float4x4 worldToLocal, out uint triangleID)
{
    float t = MaxDist;
    ro = mul(worldToLocal, float4(ro, 1.)).xyz;
    rd = mul((float3x3) worldToLocal, rd);
    uint start = mesh.indexStart;
    uint length = start + mesh.indexLength;
    float3 v0, v1, v2;
    triangleID = start;
    for (uint i = start; i < length; i += 3)
    {
        v0 = _Vertexes[_Indices[i]];
        v1 = _Vertexes[_Indices[i + 1]];
        v2 = _Vertexes[_Indices[i + 2]];

        float t1, u, v;
        if (IntersectTriangle_MT97(ro, rd, v0, v1, v2, t1, u, v))
        {
            if (t1 > eps && t1 < t)
            {
                t = t1;
                triangleID = i;
            }
        }
    }
    return t;
}
motionHit MotionTrace(in Ray r, bool prev)
{
    float t;
    uint count, stride;
    motionHit hit;
    hit.distance = MaxDist;

    _Lights.GetDimensions(count, stride);
    for (uint l = 0; l < count; l++)
    {
        if (_Lights[l].type == LightPoint)
        {
            if (prev)
                t = iSphere(r.origin, r.direction, float4(_Lights[l].prevPos, _Lights[l].scale));
            else
                t = iSphere(r.origin, r.direction, float4(_Lights[l].pos, _Lights[l].scale));
        }
        else if (_Lights[l].type == LightArea)
        {
            if (prev)
                t = IntersectDisk(r.origin, r.direction, _Lights[l].prevPos, -_Lights[l].dir, _Lights[l].scale);
            else
                t = IntersectDisk(r.origin, r.direction, _Lights[l].pos, -_Lights[l].dir, _Lights[l].scale);
        }
        if (t > eps && t < hit.distance)
        {
            hit.distance = t;
            hit.type = 1;
            hit.objectID = l;
        }
    }
    
    _Spheres.GetDimensions(count, stride);
    for (uint s = 0; s < count; s++)
    {
        if (prev)
            t = iSphere(r.origin, r.direction, float4(_Spheres[s].prevPos, _Spheres[s].scale));
        else
            t = iSphere(r.origin, r.direction, float4(_Spheres[s].pos, _Spheres[s].scale));
        
        if (t > eps && t < hit.distance)
        {
            hit.distance = t;
            hit.type = 2;
            hit.objectID = s;
        }
    }

    uint tmpTril;
    AABB tmpAABB;
    _Meshes.GetDimensions(count, stride);
    for (uint m = 0; m < count; m++)
    {
        if (prev)
            tmpAABB = _Meshes[m].prevbounds;
        else
            tmpAABB = _Meshes[m].bounds;
        if (Intersect(r.origin, rcp(r.direction), tmpAABB, eps, hit.distance))
        {
            if (prev)
                t = MotionMesh(r.origin, r.direction, _Meshes[m], _Meshes[m].prevWorldToLocal, tmpTril);
            else
                t = MotionMesh(r.origin, r.direction, _Meshes[m], _Meshes[m].worldToLocal, tmpTril);
            
            if (t > eps && t < hit.distance)
            {
                hit.distance = t;
                hit.type = 3;
                hit.objectID = m;
                hit.triangleID = tmpTril;
            }
        }
    }
    return hit;
}
float GetRR(motionHit hit, Ray ray)
{
    int index = -1;
    float2 uv = 0.;
    if (hit.type == 2)
    {
        index = _Spheres[hit.objectID].mat;
        float3 normal = nSphere(ray.origin + hit.distance * ray.direction, float4(_Spheres[hit.objectID].prevPos, _Spheres[hit.objectID].scale));
        uv = float2(acos(normal.y) / -PI, atan2(normal.x, -normal.z) / -PI * 0.5f);
    }
    else if (hit.type == 3)
    {
        index = _Meshes[hit.objectID].mat;
        float t1, u, v;
        float3 ro = mul(_Meshes[hit.objectID].prevWorldToLocal, float4(ray.origin, 1.)).xyz;
        float3 rd = mul((float3x3) _Meshes[hit.objectID].prevWorldToLocal, ray.direction);
        IntersectTriangle_MT97(ro, rd, _Vertexes[_Indices[hit.triangleID]], _Vertexes[_Indices[hit.triangleID + 1]], _Vertexes[_Indices[hit.triangleID + 2]], t1, u, v);
        uv = lerp(lerp(_UVs[_Indices[hit.triangleID]], _UVs[_Indices[hit.triangleID + 1]], u), _UVs[_Indices[hit.triangleID + 2]], v);
    }
    
    if (index == -1)
        return 1.;
    
    if (_Materials[index].type == MatSpecular || _Materials[index].type == MatDielectric)
    {
        float roughness = _Materials[index].roughness;
        if (_Materials[index].MetalTextureID != -1)
            roughness *= (1. - _RTextures.SampleLevel(sampler_RTextures, float3(uv * _Materials[index].scaling, _Materials[index].MetalTextureID), hit.distance * _DistanceLOD));
        return roughness;
    }
   
    return 1.;
}

[numthreads(kCSGroupSizeX, kCSGroupSizeY, 1)]
void RTColor(uint3 id : SV_DispatchThreadID)
{
    // Get the dimensions of the RenderTexture
    uint width, height;
    Result.GetDimensions(width, height);
    
    // Transform pixel to [-1,1] range
    float2 uv = float2((id.xy) / float2(width, height) * 2.0 - 1.0);
    Ray ray = CreateCameraRay(uv, _CameraToWorld, _CameraInverseProjection);
    
    //animated bluenoise with the goldenratio
    float seed = bluenoise((id.xy) / float2(width, height) + PHI * (_Time % 100)) + PHI * (_Time % 100);
    
    //------------------------------------------------------------
    //- PATH TRACING
    
    float3 result = 0;
    uint lightCount, stride;
    _Lights.GetDimensions(lightCount, stride);
    for (int i = 0; i < SAMPLES_PER_PIXEL; i++)
    {
        result += Trace(lightCount, ray, seed);
        //seed = frac(seed * PHI);
    }
    result /= float(SAMPLES_PER_PIXEL);
    result = pow(clamp(result, 0., 2.), .45); // enable/disable this line, to make it more or less washed out
    
    //--------------------------------------------------------------
    // TEMPORAL REPROJECTION
    float W_val = 1.;
    float2 repro = MotionVectors[id.xy];
    if (repro.x > 0 && repro.x < 1. && repro.y > 0 && repro.y < 1.)
    {
        float4 col = Previous.SampleLevel(sampler_Previous, repro, 0);
        float fract = frac(col.w);
        col.w = min(round(col.w), REPROJ_MAXFRAMES);
        W_val += col.w; //accumolate
        result = ((col.xyz * col.w) + result) / W_val;
        W_val += fract;
    }
    
    Result[id.xy] = float4(result, W_val);
}

[numthreads(kCSGroupSizeX, kCSGroupSizeY, 1)]
void RTMotion(uint3 id : SV_DispatchThreadID)
{
    // Get the dimensions of the RenderTexture
    uint width, height;
    MotionVectors.GetDimensions(width, height);
    
    // Transform pixel to [-1,1] range
    float2 uv = float2((id.xy) / float2(width, height) * 2.0 - 1.0);
    Ray ray = CreateCameraRay(uv, _CameraToWorld, _CameraInverseProjection);
    
    motionHit NewHit = MotionTrace(ray, false);

    if (NewHit.distance < MaxDist)
    {
        float3 posWS = ray.origin + NewHit.distance * ray.direction;
        if (NewHit.type == 1)
        {
            posWS += (_Lights[NewHit.objectID].prevPos - _Lights[NewHit.objectID].pos);
        }
        else if (NewHit.type == 2)
        {
            posWS += (_Spheres[NewHit.objectID].prevPos - _Spheres[NewHit.objectID].pos);
        }
        else if (NewHit.type == 3)
        {
            posWS = mul(_Meshes[NewHit.objectID].worldToLocal, float4(posWS, 0.)).xyz;
            posWS = mul((float3x3) _Meshes[NewHit.objectID].prevLocalToWorld, posWS);
        }
        float4 hPosOld = mul(_oldVP, float4(posWS, 1));
        float2 repro = (hPosOld.xy / hPosOld.w);

        ray = CreateCameraRay(repro, _old_CameraToWorld, _old_CameraInverseProjection);
        motionHit BeforeHit = MotionTrace(ray, true);

        if (Equal(NewHit, BeforeHit)) //if (length((ray.origin + BeforeHit.distance * ray.direction) - posWS) < REPROJ_DELTA)
        {
            //check if it's reflective and if there is any change in camera and object position
            //until we add reflection motion vectors, included in the MotionVector RT
            if (abs(NewHit.distance - BeforeHit.distance) > eps && GetRR(BeforeHit, ray) < .1)
            {
                MotionVectors[id.xy] = float2(-1, -1);
                return;
            }
            
            //convert back to [0,1] range, then add a small value to stabilize 
            repro = repro * .5 + .5;
            repro += 1. / float2(width * 2, height * 2);
            MotionVectors[id.xy] = repro;
            return;
        }
    }
    //invalid value, means we can't reporject basically.. (all checks are done above)
    MotionVectors[id.xy] = float2(-1, -1);
}