﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using UnityEngine;
using Rem.Rendering.PathTracing;
using Rem.Rendering.BVH;

namespace Rem.Rendering
{
    public class RenderBuffers : IDisposable
    {
        #region Properties
        public bool isReady
        {
            get =>
                _Materials != null &&
                _Lights != null &&
                _Spheres != null &&

                _Meshes != null &&
                _Vertexes != null &&
                _UVs != null &&
                _Indices != null;
        }
        #endregion

        #region Fields
        public Matrix4x4 oldVP;
        public Matrix4x4 old_toWorld;
        public Matrix4x4 old_inverse;

        public ComputeBuffer _Materials;
        public ComputeBuffer _Lights;
        public ComputeBuffer _Spheres;

        public ComputeBuffer _Meshes;
        public ComputeBuffer _Vertexes;
        public ComputeBuffer _Normals;
        public ComputeBuffer _UVs;
        public ComputeBuffer _Indices;
        #endregion

        #region Main Methods
        public void UpdateOldMatrix(Matrix4x4 pv, Camera camera)
        {
            oldVP = pv * camera.worldToCameraMatrix;
            old_inverse = pv.inverse;
            old_toWorld = camera.cameraToWorldMatrix;
        }
        public void SetMeshEmpty()
        {
            UpdateData(ref _Meshes, new MeshObject[] { new MeshObject() });
            UpdateData(ref _Vertexes, new float3[] { float3.zero, float3.zero, float3.zero });
            UpdateData(ref _Normals, new float3[] { float3.zero, float3.zero, float3.zero });
            UpdateData(ref _UVs, new float2[] { float2.zero, float2.zero, float2.zero });
            UpdateData(ref _Indices, new int[] { 0, 1, 2 });
        }

        public void SetBuffers(Scene scene)
        {
            UpdateData(ref _Materials, scene.Materials);
            UpdateData(ref _Lights, scene.Lights);
            UpdateData(ref _Spheres, scene.Spheres);

            if (scene.Meshes.IsCreated && scene.Meshes.Length > 0)
            {
                UpdateData(ref _Meshes, scene.Meshes);
                UpdateData(ref _Vertexes, scene.Vertexes);
                UpdateData(ref _Normals, scene.Normals);
                UpdateData(ref _UVs, scene.UVs);
                UpdateData(ref _Indices, scene.Indices);
            }
            else
            {
                SetMeshEmpty();
            }
        }
        public void PushToShader(ComputeShader shader)
        {
            // Required for both motion-vectors and rendering passes
            // Textures are moved to the RenderPipeline.cs
            for (int i = 0; i < 2; i++)
            {
                shader.SetBuffer(i, "_Materials", _Materials);
                shader.SetBuffer(i, "_Lights", _Lights);
                shader.SetBuffer(i, "_Spheres", _Spheres);

                shader.SetBuffer(i, "_Meshes", _Meshes);
                shader.SetBuffer(i, "_Vertexes", _Vertexes);
                shader.SetBuffer(i, "_Normals", _Normals);
                shader.SetBuffer(i, "_UVs", _UVs);
                shader.SetBuffer(i, "_Indices", _Indices);
            }
        }

        public void Dispose()
        {
            if (_Materials != null) _Materials.Dispose();
            if (_Lights != null) _Lights.Dispose();
            if (_Spheres != null) _Spheres.Dispose();
            if (_Meshes != null) _Meshes.Dispose();
            if (_Vertexes != null) _Vertexes.Dispose();
            if (_Normals != null) _Normals.Dispose();
            if (_UVs != null) _UVs.Dispose();
            if (_Indices != null) _Indices.Dispose();
        }
        #endregion

        #region Helper methods
        public unsafe static void UpdateData<T>(ref ComputeBuffer cb, NativeArray<T> array) where T : unmanaged
        {
            if (cb != null)
                cb.Release();
            if (array.Length > 0)
            {
                cb = new ComputeBuffer(array.Length, UnsafeUtility.SizeOf<T>(), ComputeBufferType.Structured);
                cb.SetData(array);
            }
        }
        public unsafe static void UpdateData<T>(ref ComputeBuffer cb, NativeList<T> array) where T : unmanaged
        {
            if (cb != null)
                cb.Release();
            if (array.Length > 0)
            {
                cb = new ComputeBuffer(array.Length, UnsafeUtility.SizeOf<T>(), ComputeBufferType.Structured);
                cb.SetData(array.ToArray());
            }
        }
        public unsafe static void UpdateData<T>(ref ComputeBuffer cb, T[] array) where T : unmanaged
        {
            if (cb != null)
                cb.Release();
            if (array.Length > 0)
            {
                cb = new ComputeBuffer(array.Length, UnsafeUtility.SizeOf<T>(), ComputeBufferType.Structured);
                cb.SetData(array);
            }
        }
        #endregion
    }
}