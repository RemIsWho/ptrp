//---------------------------
//- HASHING

float hash1(inout float seed)
{
    return frac(sin(seed += 0.1) * 43758.5453123);
}

float2 hash2(inout float seed)
{
    return frac(sin(float2(seed += 0.1, seed += 0.1)) * float2(43758.5453123, 22578.1459123));
}

float3 hash3(inout float seed)
{
    return frac(sin(float3(seed += 0.1, seed += 0.1, seed += 0.1)) * float3(43758.5453123, 22578.1459123, 19642.3490423));
}

//-------------------------
//- RANDOM
float3 cosWeightedRandomHemisphereDirection(const float3 n, inout float seed)
{
    float2 r = hash2(seed);
    
    float3 uu = normalize(cross(n, float3(0., 1., 1.)));
    float3 vv = cross(uu, n);
	
    float ra = sqrt(r.y);
    float rx = ra * cos(6.28318530718 * r.x);
    float ry = ra * sin(6.28318530718 * r.x);
    float rz = sqrt(1. - r.y);
    float3 rr = float3(rx * uu + ry * vv + rz * n);
    
    return normalize(rr);
}

float3 randomSphereDirection(inout float seed)
{
    float2 h = hash2(seed) * float2(2., 6.28318530718) - float2(1., 0.);
    float phi = h.y;
    return float3(sqrt(1. - h.x * h.x) * float2(sin(phi), cos(phi)), h.x);
}

float3 randomHemisphereDirection(const float3 n, inout float seed)
{
    float3 dr = randomSphereDirection(seed);
    return dot(dr, n) * dr;
}